//
//  AddCarViewController.m
//  MyCars
//
//  Created by Aquiles Alfaro on 12/15/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "AddCarViewController.h"
#import "Vehicle+CoreDataClass.h"

@interface AddCarViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtMake;
@property (weak, nonatomic) IBOutlet UITextField *txtModel;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;
@property (weak, nonatomic) IBOutlet UITextField *txtMPG;

@end

@implementation AddCarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate = (AppDelegate*) [[UIApplication sharedApplication]delegate];
    context = appDelegate.persistentContainer.viewContext;
    
    if(self.aCar){
        self.txtMake.text = [self.aCar valueForKey:@"make"];
        self.txtModel.text = [self.aCar valueForKey:@"model"];
        // get the year from the db and convert it to a string
        NSNumber *numericYear = [self.aCar valueForKey:@"year"];
        NSString *year = [NSString stringWithFormat:@"%@", numericYear];
        self.txtYear.text = year;
        // get the mpg from the db and convert it to a string
        NSNumber *numericMPG = [self.aCar valueForKey:@"mpg"];
        NSString *mpg = [NSString stringWithFormat:@"%@", numericMPG];
        self.txtMPG.text = mpg;
        
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    context = appDelegate.persistentContainer.viewContext;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)saveRecord:(UIButton *)sender {
    
    // initialize formatter
    NSNumberFormatter *f = [[NSNumberFormatter alloc]init];
    
    if (self.aCar){
    [self.aCar setValue:self.txtMake.text forKey:@"make"];
    [self.aCar setValue:self.txtModel.text forKey:@"model"];
    // set the year format to integer
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber *myYear = [f numberFromString:self.txtYear.text];
    [self.aCar setValue:myYear forKey:@"year"];
    
    // set the mpg
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber *myMPG = [f numberFromString:self.txtMPG.text];
    [self.aCar setValue:myMPG forKey:@"mpg"];
    
    }else{
     // New Car
    Vehicle *myCar = [[Vehicle alloc]initWithContext:context];
        [myCar setValue:self.txtMake.text forKey:@"make"];
        [myCar setValue:self.txtModel.text forKey:@"model"];
        
        [f setNumberStyle:NSNumberFormatterNoStyle];
        NSNumber *myYear = [f numberFromString:self.txtYear.text];
        [myCar setValue:myYear forKey:@"year"];
        
        [f setNumberStyle:NSNumberFormatterNoStyle];
        NSNumber *myMPG = [f numberFromString:self.txtMPG.text];
        [myCar setValue:myMPG forKey:@"mpg"];
    }
    // zero out the fields
    self.txtMake.text = @"";
    self.txtModel.text = @"";
    self.txtYear.text = @"";
    self.txtMPG.text = @"";
    
    // commit
    NSError *error = nil;
    
    if (![context save:&error]){
        NSLog(@"%@, %@", error, [error localizedDescription]);
    }
    
    
    // dismiss the view
        [self.navigationController popViewControllerAnimated:YES];
    
    
}
- (IBAction)dismissKeyboard:(id)sender {
}

@end
