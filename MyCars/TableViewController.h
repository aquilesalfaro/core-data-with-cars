//
//  TableViewController.h
//  MyCars
//
//  Created by Aquiles Alfaro on 12/15/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface TableViewController : UITableViewController
{
    AppDelegate *appDelegate;
    NSManagedObjectContext * context;
}

@end
